package Tugas1;

public class MasyarakatSekitar extends Penduduk {

    private String nomor;

    public MasyarakatSekitar() {}

    public MasyarakatSekitar(String nomor, String nama, String tanggalLahir) {
        super(nama, tanggalLahir);
        this.nomor = nomor;
    }

    public void setNomor(String nomor) {
        this.nomor = nomor;
    }

    public String getNomor() {
        return nomor;
    }

    @Override
    public double hitungIuran() {
        double hitungIuran;
        double konvNumb = Double.parseDouble(nomor);
        hitungIuran = konvNumb / 100;
        return hitungIuran;
    }
}

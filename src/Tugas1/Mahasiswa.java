package Tugas1;

public class Mahasiswa extends Penduduk {

    private String nim;

    public Mahasiswa() {}

    public Mahasiswa(String nim, String nama, String tanggalLahir) {
        super(nama, tanggalLahir);
        this.nim = nim;
    }

    public void setNim(String nim) {
        this.nim = nim;
    }

    public String getNim() {
        return nim;
    }

    @Override
    public double hitungIuran() {
        double hitungIuran;
        double konvNumb = Double.parseDouble(nim);
        hitungIuran = konvNumb / 10000;
        return hitungIuran;
    }

}

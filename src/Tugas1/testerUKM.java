package Tugas1;

import java.util.Scanner;

public class testerUKM {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        UKM ukm = new UKM();

        System.out.println("Silahkan masukan nama UKM di bawah ini");
        System.out.print("Nama UKM  : ");
        ukm.setNamaUnit(input.nextLine().toUpperCase());
        System.out.println("");
        System.out.println("------------------------------------------------------------");
        System.out.println("\t\t\tUKM " + ukm.getNamaUnit());

        Mahasiswa ketua = new Mahasiswa();
        System.out.println("Ketua " + ukm.getNamaUnit());
        ketua.setNama("Erick Tarigas");
        ketua.setNim("164089093");
        ketua.setTanggalLahir("19 September 1997");

        System.out.println("Nama      : " + ketua.getNama());
        System.out.println("NIM       : " + ketua.getNim());
        System.out.println("Tgl lahir : " + ketua.getTanggalLahir());
        ukm.setKetua(ketua);
        System.out.println("");

        Mahasiswa sekretaris = new Mahasiswa();
        System.out.println("Sekretaris " + ukm.getNamaUnit());
        sekretaris.setNama("Valenia Angel");
        sekretaris.setNim("1786543107");
        sekretaris.setTanggalLahir("23 Januari 2000");

        System.out.println("Nama      : " + sekretaris.getNama());
        System.out.println("NIM       : " + sekretaris.getNim());
        System.out.println("Tgl lahir : " + sekretaris.getTanggalLahir());
        ukm.setSekretaris(sekretaris);
        System.out.println("");

        System.out.println("------------------------------------------------------------");//        
        System.out.print("Jumlah mahasiswa yang mendaftar sebagai anggota UKM : ");
        int jumlah = input.nextInt();
        Mahasiswa mhs = new Mahasiswa();

        System.out.println("");
        for (int i = 0; i < jumlah; i++) {
            System.out.print("NIM         : ");
            mhs.setNim(input.next());
            mhs.setNama(input.nextLine());
            System.out.print("Nama        : ");
            mhs.setNama(input.next());
            mhs.setTanggalLahir(input.nextLine());
            System.out.print("Tgl lahir   : ");
            mhs.setTanggalLahir(input.next());
            System.out.println("");
        }
        System.out.println("------------------------------------------------------------");

        MasyarakatSekitar[] anggota = new MasyarakatSekitar[3];

        anggota[0] = new MasyarakatSekitar("101", "Henri Gohun", "8 Desember 1989");
        anggota[1] = new MasyarakatSekitar("152", "Gania Anastasia", "11 Oktober 1997");
        anggota[2] = new MasyarakatSekitar("193", "Maro Marco", "30 Mei 2000");
        for (int i = 0; i < anggota.length; i++) {
            System.out.println("Tahun bergabung       : " + anggota[i].getNomor());
            System.out.println("*(3 digit, 2 digit angka di depan adalah tahun masuk kemudian nomor urut anggota)");
            System.out.println("Nomor dan Nama        : " + anggota[i].getNama());
            System.out.println("Tanggal lahir         : " + anggota[i].getTanggalLahir());
            ukm.setAnggota(anggota);
            System.out.println("");
        }

        double hitung1 = 0;
        double hitung2 = 0;
        double tampungHit = 0;

        System.out.println("------------------------------------------------------------");
        System.out.println("");
        System.out.println("Anggota UKM " + ukm.getNamaUnit() + " yang merupakan mahasiswa :");
        System.out.println("======================================================================================================");
        System.out.printf("%-5s", "No.");
        System.out.printf("%-15s", "NIM");
        System.out.printf("%-28s", "Nama");
        System.out.printf("%-18s", "Iuran/bulan (Rp)");
        System.out.println("");
        System.out.println("-------------------------------------------------------------------------------------------------------");
        for (int i = 0; i < jumlah; i++) {
            System.out.printf("%-5s", (i + 1) + ".");
            System.out.printf("%-15s", mhs.getNim());
            System.out.printf("%-28s", mhs.getNama());
            System.out.printf("%-18s", mhs.hitungIuran());
            hitung1 = mhs.hitungIuran();
            System.out.println("");
        }
        System.out.println("======================================================================================================");
        System.out.println("");
        System.out.println("Anggota UKM " + ukm.getNamaUnit() + " yang merupakan masyarakat sekitar :");
        System.out.println("======================================================================================================");
        System.out.printf("%-5s", "No.");
        System.out.printf("%-10s", "Nomor");
        System.out.printf("%-18s", "Nama");
        System.out.printf("%-18s", "Iuran/bulan (Rp)");
        System.out.println("");
        System.out.println("-------------------------------------------------------------------------------------------------------");
        for (int i = 0; i < anggota.length; i++) {
            System.out.printf("%-5s", (i + 1) + ".");
            System.out.printf("%-10s", anggota[i].getNomor());
            System.out.printf("%-18s", anggota[i].getNama());
            System.out.printf("%-18s", anggota[i].hitungIuran());
            hitung2 = anggota[i].hitungIuran();
            System.out.println("");
        }
        System.out.println("=======================================================================================================");
        tampungHit = hitung1 + hitung2;
        System.out.println("TOTAL                                                                        = " + tampungHit);
    }
}

package Praktikum2_2;

public class Manajer extends Pegawai implements TugasBelajar {

    private int jumlahAnak;
    private int jamKerja;
    private int istri;

    public Manajer() {
        super();
    }

    public Manajer(int jumlahAnak, int jamKerja, int istri) {
        this.jumlahAnak = jumlahAnak;
        this.jamKerja = jamKerja;
        this.istri = istri;
    }

    public Manajer(int jumlahAnak, int jamKerja, int istri, String nip, String nama, int golongan) {
        super(nip, nama, golongan);
        this.jumlahAnak = jumlahAnak;
        this.jamKerja = jamKerja;
        this.istri = istri;
    }

    public void setJumlahAnak(int jumlahAnak) {
        this.jumlahAnak = jumlahAnak;
    }

    public void setJamKerja(int jamKerja) {
        this.jamKerja = jamKerja;
    }

    public void setIstri(int istri) {
        this.istri = istri;
    }

    public int getJumlahAnak() {
        return jumlahAnak;
    }

    public int getJamKerja() {
        return jamKerja;
    }

    public int getIstri() {
        return istri;
    }

    @Override
    public boolean isSelesai() {
        return true;
    }

    public double hitungTunjangan() {
        double tunjangan = 0;
        if (this.jumlahAnak <= 3) {
            tunjangan = this.jumlahAnak * 100000;
        } else if (this.jumlahAnak > 3) {
            return 0;
        }
        return tunjangan;
    }

    public double hitLembur() {
        double lembur = 0;
        if (this.jamKerja > 8) {
            lembur = (this.jamKerja - 8) * 50000;
        }
        return lembur;
    }

    @Override
    public double hitungGatot() {
        return (hitGajiPokok() + hitungTunjangan() + hitLembur());
    }
}

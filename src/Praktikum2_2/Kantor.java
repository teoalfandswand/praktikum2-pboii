package Praktikum2_2;

public class Kantor {
    private Manajer manajer;
    private Pegawai[] pegawai;

    public Kantor(){}
    
    public Kantor(Manajer manajer, Pegawai[] pegawai) {
        this.manajer = manajer;
        this.pegawai = pegawai;
    }

    public void setManajer(Manajer manajer) {
        this.manajer = manajer;
    }

    public void setPegawai(Pegawai[] pegawai) {
        this.pegawai = pegawai;
    }

    public Manajer getManajer() {
        return manajer;
    }

    public Pegawai[] getPegawai() {
        return pegawai;
    }
    
    public double hitGajiPeg(){
        double gajiPeg = 0;
        return gajiPeg;
    }
}

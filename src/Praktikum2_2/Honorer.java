package Praktikum2_2;

public class Honorer extends Pegawai {

    public Honorer() {}

    public Honorer(String nip, String nama, int golongan) {
        super(nip, nama, golongan);
    }

    @Override
    public double hitungGatot() {
        return hitGajiPokok();
    }

}

package Praktikum2_2;

public class Pegawai implements Pendapatan{

    protected String nip;
    protected String nama;
    protected int golongan;
    protected double gajiTotal;

    public Pegawai() {
    }

    public Pegawai(String nip, String nama, int golongan) {
        this.nip = nip;
        this.nama = nama;
        this.golongan = golongan;
    }

    public void setNip(String nip) {
        this.nip = nip;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public void setGolongan(int golongan) {
        this.golongan = golongan;
    }

    public String getNip() {
        return nip;
    }

    public String getNama() {
        return nama;
    }

    public int getGolongan() {
        return golongan;
    }

    public double hitGajiPokok() {
        switch (golongan) {
            case 1:
                return 500000;
            case 2:
                return 750000;
            case 3:
                return 1000000;
        }
        return hitGajiPokok();
    }

    @Override
    public double hitungGatot(){
        double gatot = 0;
        return gatot;
    }
}

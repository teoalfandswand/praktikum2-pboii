package Praktikum2_2;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Scanner;

public class MainPegawaiKantor {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        Kantor kantor = new Kantor();
        Manajer mnjr = new Manajer();
        
        System.out.print("Nama  : ");
        String nama = input.nextLine();
        mnjr.setNip("P.10888");
        mnjr.setNama(nama);
        mnjr.setGolongan(3);
        mnjr.setJamKerja(12);
        mnjr.setIstri(1);
        mnjr.setJumlahAnak(3);      
        kantor.setManajer(mnjr);
        
        Pegawai[] pgw = new Pegawai[3];
        pgw[0] = mnjr;
        pgw[1] = new Marketing(10, "P.12347", "Verry", 2);
        pgw[2] = new Honorer("P.12765", "Dania", 1);
        kantor.setPegawai(pgw);
        
        DecimalFormat kursIndonesia = (DecimalFormat) DecimalFormat.getCurrencyInstance();
        DecimalFormatSymbols formatRp = new DecimalFormatSymbols();

        formatRp.setCurrencySymbol("Rp ");
        formatRp.setMonetaryDecimalSeparator(',');
        formatRp.setGroupingSeparator(',');

        kursIndonesia.setDecimalFormatSymbols(formatRp);
        
        double hitung;
        double tampungHit = 0;
        
        System.out.println("                                    DAFTAR PEGAWAI" );
        System.out.println("=======================================================================");
        System.out.printf("%-5s", "No.");
        System.out.printf("%-10s", "NIP");
        System.out.printf("%-15s", "Nama");
        System.out.printf("%-10s", "Studi");
        System.out.printf("%-20s", "Gaji");
        System.out.println("");
        System.out.println("-----------------------------------------------------------------------");
        for (int i = 0; i < pgw.length; i++) {
            if (kantor.getPegawai()[i] instanceof Manajer) {
                System.out.printf("%-5s", (i + 1) + ".");
                System.out.printf("%-10s", kantor.getManajer().getNip());
                System.out.printf("%-15s", kantor.getManajer().getNama());
                if(mnjr.isSelesai() == true){
                    System.out.printf("%-10s", "Selesai");
                } 
                System.out.printf("%-20s", kursIndonesia.format(kantor.getManajer().hitungGatot()));
                tampungHit += kantor.getManajer().hitungGatot();
                System.out.println("");
            } else if (kantor.getPegawai()[i] instanceof Marketing) {
                System.out.printf("%-5s", (i + 1) + ".");
                System.out.printf("%-10s", kantor.getPegawai()[i].getNip());
                System.out.printf("%-15s", kantor.getPegawai()[i].getNama());
                if(mnjr.isSelesai() == true){
                    System.out.printf("%-10s", "Studi");
                } 
                System.out.printf("%-20s", kursIndonesia.format(kantor.getPegawai()[i].hitungGatot()));
                tampungHit += kantor.getPegawai()[i].hitungGatot();
                System.out.println("");
            } else if(kantor.getPegawai()[i] instanceof Honorer){
                System.out.printf("%-5s", (i + 1) + ".");
                System.out.printf("%-10s", kantor.getPegawai()[i].getNip());
                System.out.printf("%-15s", kantor.getPegawai()[i].getNama());
                System.out.printf("%-10s", "-");
                System.out.printf("%-20s", kursIndonesia.format(kantor.getPegawai()[i].hitungGatot()));
                tampungHit += kantor.getPegawai()[i].hitungGatot();
                System.out.println("");
            }
        }
        hitung = tampungHit;
        System.out.println("-----------------------------------------------------------------------");
        System.out.println("Total                                                = " + kursIndonesia.format(hitung));
        System.out.println("=======================================================================");
    
    }
}

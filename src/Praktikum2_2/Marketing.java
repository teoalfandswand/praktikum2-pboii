package Praktikum2_2;

public class Marketing extends Pegawai implements TugasBelajar {

    private int jamKerja;

    public Marketing() {
        super();
    }

    public Marketing(int jamKerja) {
        this.jamKerja = jamKerja;
    }

    public Marketing(int jamKerja, String nip, String nama, int golongan) {
        super(nip, nama, golongan);
        this.jamKerja = jamKerja;
    }

    public void setJamKerja(int jamKerja) {
        this.jamKerja = jamKerja;
    }

    public int getJamKerja() {
        return jamKerja;
    }

    @Override
    public boolean isSelesai() {
        return true;
    }

    public double hitLembur() {
        double lembur = 0;
        if (this.jamKerja > 8) {
            lembur = (this.jamKerja - 8) * 50000;
        }
        return lembur;
    }

    @Override
    public double hitungGatot() {
        return (hitGajiPokok() + hitLembur());
    }

}

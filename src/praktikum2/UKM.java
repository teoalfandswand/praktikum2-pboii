package praktikum2;

public class UKM {

    private String namaUnit;
    private Mahasiswa ketua;
    private Mahasiswa sekretaris;
    private Penduduk[] anggota;

    public UKM() {

    }

    public void setNamaUnit(String namaUnit) {
        this.namaUnit = namaUnit;
    }

    public void setKetua(Mahasiswa ketua) {
        this.ketua = ketua;
    }

    public void setSekretaris(Mahasiswa sekretaris) {
        this.sekretaris = sekretaris;
    }

    public void setAnggota(Penduduk[] anggota) {
        this.anggota = anggota;
    }

    public String getNamaUnit() {
        return namaUnit;
    }

    public Mahasiswa getKetua() {
        return ketua;
    }

    public Mahasiswa getSekretaris() {
        return sekretaris;
    }

    public Penduduk[] getAnggota() {
        return anggota;
    }

}

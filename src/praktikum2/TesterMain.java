package praktikum2;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Scanner;

public class TesterMain {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        UKM ukm = new UKM();

        System.out.print("Nama UKM              : ");
        String namaUKM = input.nextLine().toUpperCase();
        ukm.setNamaUnit(namaUKM);
        System.out.println("====================================================");
        System.out.print("Nama Ketua UKM        : ");
        String namaKetua = input.next();
        System.out.print("Nama Sekretaris UKM   : ");
        String namaSekretaris = input.next();

        Mahasiswa ketua = new Mahasiswa();
        ukm.setKetua(ketua);
        ketua.setNama(namaKetua);
        ketua.setNim("185314105");
        ketua.setTanggalLahir("8 Desember 2000");

        Mahasiswa sekretaris = new Mahasiswa();
        ukm.setSekretaris(sekretaris);
        sekretaris.setNama(namaSekretaris);
        sekretaris.setNim("184410308");
        sekretaris.setTanggalLahir("6 Juni 2000");

        Penduduk[] anggota = new Penduduk[5];
        Mahasiswa mhs = new Mahasiswa();
        Masyarakat mas = new Masyarakat();

        ukm.setAnggota(anggota);
        anggota[0] = ketua;
        anggota[1] = sekretaris;
        anggota[2] = new Masyarakat("111", "Julian", "11 Novemeber 1991");
        anggota[3] = new Masyarakat("106", "Agnes", "7 Januari 2002");
        anggota[4] = new Mahasiswa("184315098", "Ronald", "30 Agustus 1999");

        DecimalFormat kursIndonesia = (DecimalFormat) DecimalFormat.getCurrencyInstance();
        DecimalFormatSymbols formatRp = new DecimalFormatSymbols();

        formatRp.setCurrencySymbol("Rp ");
        formatRp.setMonetaryDecimalSeparator(',');
        formatRp.setGroupingSeparator(',');

        kursIndonesia.setDecimalFormatSymbols(formatRp);

        double hitung;
        double tampungHit = 0;

        System.out.println("\n");
        System.out.println("                            DAFTAR ANGGOTA UKM " + ukm.getNamaUnit());
        System.out.println("=============================================================================================================================");
        System.out.printf("%-5s", "No.");
        System.out.printf("%-15s", "Nama");
        System.out.printf("%-15s", "Iuran");
        System.out.printf("%-15s", "Sertifikat");
        System.out.printf("%-43s", "Fasilitas");
        System.out.printf("%-35s", "Konsumsi");
        System.out.println("");
        System.out.println("-----------------------------------------------------------------------------------------------------------------------------");
        for (int i = 0; i < anggota.length; i++) {
            if (ukm.getAnggota()[i] instanceof Mahasiswa) {
                System.out.printf("%-5s", (i + 1) + ".");
                System.out.printf("%-15s", ukm.getAnggota()[i].getNama());
                if (ukm.getAnggota()[i].equals(ketua) || ukm.getAnggota()[i].equals(sekretaris)) {
                    double hitungIuran = 0;
                    System.out.printf("%-15s", hitungIuran);
                } else {
                    System.out.printf("%-15s", kursIndonesia.format(ukm.getAnggota()[i].hitungIuran()));
                }
                System.out.printf("%-15s", mhs.getJenisSertifikat());
                System.out.printf("%-43s", mhs.getFasilitas());
                System.out.printf("%-35s", mhs.getKonsumsi());
                tampungHit += ukm.getAnggota()[i].hitungIuran();
                System.out.println("");
            } else if (ukm.getAnggota()[i] instanceof Masyarakat) {
                System.out.printf("%-5s", (i + 1) + ".");
                System.out.printf("%-15s", ukm.getAnggota()[i].getNama());
                System.out.printf("%-15s", kursIndonesia.format(ukm.getAnggota()[i].hitungIuran()));
                System.out.printf("%-15s", mas.getJenisSertifikat());
                System.out.printf("%-43s", mas.getFasilitas());
                System.out.printf("%-35s", mas.getKonsumsi());
                System.out.println("");
                tampungHit += ukm.getAnggota()[i].hitungIuran();
            }
        }
        hitung = tampungHit;
        System.out.println("-----------------------------------------------------------------------------------------------------------------------------");
        System.out.println("Total                                                                                               = " + kursIndonesia.format(hitung));
        System.out.println("=============================================================================================================================");
    }
}

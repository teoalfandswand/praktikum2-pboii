package praktikum2;

public interface Peserta {
    public String getJenisSertifikat();
    public String getFasilitas();
    public String getKonsumsi();
}

package praktikum2;

public class Masyarakat extends Penduduk implements Peserta {

    private String nomor;

    public Masyarakat() {
    }

    public Masyarakat(String nomor) {
        this.nomor = nomor;
    }

    public Masyarakat(String nomor, String nama, String tanggalLahir) {
        super(nama, tanggalLahir);
        this.nomor = nomor;
    }

    public void setNomor(String nomor) {
        this.nomor = nomor;
    }

    public String getNomor() {
        return nomor;
    }

    @Override
    public double hitungIuran() {
        return (Double.parseDouble(nomor) * 100);
    }

    @Override
    public String getJenisSertifikat() {
        String jenisSertifikat = "Peserta";
        return jenisSertifikat;
    }

    @Override
    public String getFasilitas() {
        String fasilitas = "block note, alat tulis, modul pelatihan";
        return fasilitas;
    }

    @Override
    public String getKonsumsi() {
        String konsumsi = "snack dan makan siang";
        return konsumsi;
    }
}

package praktikum2;

public abstract class Penduduk {

    protected String nama;
    protected String tanggalLahir;

    public Penduduk() {
    }

    public Penduduk(String nama, String tanggalLahir) {
        this.nama = nama;
        this.tanggalLahir = tanggalLahir;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public void setTanggalLahir(String tanggalLahir) {
        this.tanggalLahir = tanggalLahir;
    }

    public String getNama() {
        return nama;
    }

    public String getTanggalLahir() {
        return tanggalLahir;
    }

    public abstract double hitungIuran();
}

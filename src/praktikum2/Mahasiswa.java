package praktikum2;

public class Mahasiswa extends Penduduk implements Peserta {

    private String nim;

    public Mahasiswa() {
    }

    public Mahasiswa(String nim) {
        this.nim = nim;
    }

    public Mahasiswa(String nim, String nama, String tanggalLahir) {
        super(nama, tanggalLahir);
        this.nim = nim;
    }

    public void setNim(String nim) {
        this.nim = nim;
    }

    public String getNim() {
        return nim;
    }

    @Override
    public double hitungIuran() {
        return (Double.parseDouble(nim) / 10000);
    }

    @Override
    public String getJenisSertifikat() {
        String jenisSertifikat = "Panitia";
        return jenisSertifikat;
    }

    @Override
    public String getFasilitas() {
        String fasilitas = "block note, alat tulis, laptop";
        return fasilitas;
    }

    @Override
    public String getKonsumsi() {
        String konsumsi = "snack, makan siang, makan malam";
        return konsumsi;
    }
}
